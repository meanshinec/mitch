# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 15:12:18 2018

@author: mitch.chang
"""


import numpy as np
import cv2
#import scipy.ndimage
#from scipy.optimize import curve_fit
import time
import matplotlib.pyplot as plt
#import heapq
#import pandas as pd







path = 'SD9391_50cm-b2.jpg'
mode = 1  # 0 = farthest charts in each direction, 1 = all charts, 0~1 = certain field


img_Original = cv2.imread(path)
height_Original, width_Original, channels_Original =  img_Original.shape
hsv = cv2.cvtColor(img_Original, cv2.COLOR_BGR2HSV)






#temp = hsv[:,:,0].tolist()
#data = []
#for ii in range(len(temp)):
#    data = data + temp[ii]
#
#plt.hist(data, bins=6, normed=0, facecolor="blue", edgecolor="black", alpha=0.7)
#plt.title('H')
#plt.show()
#
#temp = hsv[:,:,1].tolist()
#data = []
#for ii in range(len(temp)):
#    data = data + temp[ii]
#
#plt.hist(data, bins=10, normed=0, facecolor="green", edgecolor="black", alpha=0.7)
#plt.title('S')
#plt.show()
#
#temp = hsv[:,:,2].tolist()
#data = []
#for ii in range(len(temp)):
#    data = data + temp[ii]
#
#plt.hist(data, bins=10, normed=0, facecolor="red", edgecolor="black", alpha=0.7)
#plt.title('V')
#plt.show()

#print('\n')
#print('RGB mean =', int(img_Original[:,:,2].mean()),
#                      int(img_Original[:,:,1].mean()),
#                      int(img_Original[:,:,0].mean()))
#
#print('hsv mean =', int(hsv[:,:,0].mean()),
#                      int(hsv[:,:,1].mean()),
#                      int(hsv[:,:,2].mean()))

print('\n')
print('hsv min =', hsv[:,:,0].min(), hsv[:,:,1].min(), hsv[:,:,2].min())
print('hsv max =',hsv[:,:,0].max(), hsv[:,:,1].max(), hsv[:,:,2].max())
